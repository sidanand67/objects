let icecream = {flavor: 'chocolate'}; 

let defaults = require('../defaults'); 

let result = defaults(icecream, { flavor: "vanilla", sprinkles: "lots" }); 

console.log(result); 