const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

let mapObject = require('../mapObject'); 

let result = mapObject(testObject, (v, k, o) => {
    if (v === 36){
        return o[k] = 25; 
    }
    return v;
}); 

console.log(result); 