function mapObject(obj, cb){
    let result = {}; 
    for(let key in obj){
        result[key] = cb(obj[key], key, obj); 
    }
    return result; 
}

module.exports = mapObject; 