function keys(obj){
    let result = []; 
    for(let key in obj){
        result.push(String(key)); 
    }
    return result; 
}

module.exports = keys; 